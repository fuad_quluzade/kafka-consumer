package az.ingress;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.header.Header;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.annotation.KafkaListener;

@SpringBootApplication
@EnableKafka
@Slf4j
public class KafkaConsumerDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(KafkaConsumerDemoApplication.class, args);
    }
    
    @KafkaListener(topics="ms-demo-topic")
    public void listener(ConsumerRecord<String,String> data){
        log.info("message from producer {}",data);
        var headers = data.headers().headers("Accept-Language").iterator();
        while(headers.hasNext()){
            log.info("header is {}",new String(headers.next().value()));
        }



    }
    

}
